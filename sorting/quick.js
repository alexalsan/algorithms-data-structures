const swap = (arr, a, b) => {
    const aux = arr[a];
    arr[a] = arr[b];
    arr[b] = aux;
};

const pivotHelper = (arr, startIndex = 0, endIndex = arr.length - 1) => {
    if (startIndex >= endIndex || !arr) return;

    let pivot = arr[startIndex];
    let swapIndex = startIndex;
    for (let i = startIndex + 1; i <= endIndex; i++) {
        if (arr[i] < pivot) {
            swapIndex++;
            swap(arr, i, swapIndex);
        }
    }
    swap(arr, startIndex, swapIndex);
    return swapIndex;
};

/*
    We call the pivotHelper on the array. Then, we recursively call the pivotHelper
    on the subarrays to the left and to the right of the pivot.\
    The base case for the recursion is when we have a subarray of 1 or 0 elements.
*/
const quickSort = (arr, left=0, right=arr.length-1) => {
    if (left < right){
        let pivotIndex = pivotHelper(arr,left,right);
        //left
        quickSort(arr,left,pivotIndex-1);
        //right
        quickSort(arr,pivotIndex+1,right);
    }
    return arr;
};

console.log(quickSort([2, 3, 5, 1, 4]));
console.log(quickSort([2, 5, 1, 9, 4, 11, 1, 7, 2, 16, 4, 1]));
console.log(quickSort([2, 5, 8, 9, 4, 11, 12, 7, 2, 16, 4, 1]));
