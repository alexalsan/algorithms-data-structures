"use strict";

//Without optimization
const bubbleSort = (arr) => {
    for (let nRounds = 0; nRounds < arr.length-1; nRounds++) {
        for (let i = 0; i < arr.length - nRounds; i++) {
            if (arr[i] > arr[i + 1]) {
                //We swap
                const aux = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = aux;
            }
        }
        console.log(arr);
        console.log(nRounds);
    }
};

bubbleSort([2,5,1,9,4,11,1,7,2,16,4,1]); //8 rounds with optimization. 10 rounds without it
bubbleSort([2,5,8,9,4,11,12,7,2,16,4,1]);

//With optimization:
//We check if we've swapped in the previous round. If we haven't, it's already sorted and we can stop.
const bubbleSort = (arr) => {
    let didWeSwap;

    for (let nRounds = 0; nRounds < arr.length-1; nRounds++) {
        didWeSwap = false;
        for (let i = 0; i < arr.length - nRounds; i++) {
            if (arr[i] > arr[i + 1]) {
                //We swap
                const aux = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = aux;
                didWeSwap = true;
            }
        }
        if (!didWeSwap) {
            break;
        }
        console.log(arr);
        console.log(nRounds);
    }
};

bubbleSort([2,5,1,9,4,11,1,7,2,16,4,1]); //8 rounds with optimization. 10 rounds without it
bubbleSort([2,5,8,9,4,11,12,7,2,16,4,1]);