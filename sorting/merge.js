const mergeArrays = (arr1, arr2) => {
    //IMPORTANT:
    //We are assuming for merge sort that both arrays are already SORTED
    let results = [];
    let i = 0;
    let j = 0;

    //We go through both arrays and take the smallest element to create the results array
    while (i < arr1.length && j < arr2.length) {
        if (arr1[i] < arr2[j]) {
            results.push(arr1[i]);
            i++;
        } else {
            results.push(arr2[j]);
            j++;
        }
    }

    //One of the arrays will finish before the other,
    //so now we just push all the elements of the remaining array
    while (i < arr1.length) {
        results.push(arr1[i]);
        i++;
    }
    while (j < arr2.length) {
        results.push(arr2[j]);
        j++;
    }

    return results;
};

//We are going to implement the sorting function using recursion
const mergeSort = (arr) => {
    //Base case of the recursion
    if (arr.length <=1) return arr;

    //We take the midpoint of the array, to divide it in 2 arrays
    const midPoint = Math.floor(arr.length/2);
    const leftHalve = mergeSort(arr.slice(0, midPoint));
    const rightHalve = mergeSort(arr.slice(midPoint));

    //Once we have splitted the array until we have individual arrays,
    //it's time to put them back together, but sorting them out.
    return mergeArrays(leftHalve, rightHalve);
}

console.log(mergeSort([5, 3, 2, 1, 4]));
console.log(mergeSort([2, 5, 1, 9, 4, 11, 1, 7, 2, 16, 4, 1]));
console.log(mergeSort([2, 5, 8, 9, 4, 11, 12, 7, 2, 16, 4, 1]));