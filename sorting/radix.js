//We get the i-th digit of a number
const getDigit = (num, i) => {
    return Math.floor(Math.abs(num) / Math.pow(10, i)) % 10;
}

//We get the number of digits a number has
const getDigitNumber = (num) => {
    if (num === 0) return 1;
    return Math.floor(Math.log10(Math.abs(num))) + 1;
}

//Given an array, we get the number of digits of the number with most digits
const getHighestDigit = (nums) => {
    let maxDigits = 0;
    for (let i = 0; i < nums.length; i++) {
        maxDigits = Math.max(maxDigits, getDigitNumber(nums[i]));
    }
    return maxDigits;
}

const radixSort = (arr) => {
    let maxDigitCount = getHighestDigit(arr);
    for (let k = 0; k < maxDigitCount; k++) {
        let digitBuckets = Array.from({ length: 10 }, () => []);
        for (let i = 0; i < arr.length; i++) {
            let digit = getDigit(arr[i], k);
            digitBuckets[digit].push(arr[i]);
        }
        arr = [].concat(...digitBuckets);
    }
    return arr;
}

console.log(radixSort([23, 345, 5467, 12, 2345, 9852]));
