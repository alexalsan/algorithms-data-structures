const selectionSort = (arr) => {
    for (i = 0; i < arr.length; i++) {
        let minIndex = i; //Index of the minimum element
        for (j = i + 1; j < arr.length; j++) {
            if (arr[j] < arr[minIndex]) minIndex = j;
        }

        if (arr[minIndex] !== arr[i]) {
            //We swap
            const aux = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = aux;
        }
        console.log("---" + i + "---");
        console.log(arr);
    }
};

selectionSort([2, 5, 1, 9, 4, 11, 1, 7, 2, 16, 4, 1]);
selectionSort([2, 5, 8, 9, 4, 11, 12, 7, 2, 16, 4, 1]);
