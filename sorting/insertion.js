const swap = (arr, a, b) => {
    const aux = arr[a];
    arr[a] = arr[b];
    arr[b] = aux;
};

const insertionSort = (arr) => {
    for (let i = 1; i < arr.length; i++) {
        //We take an element and compare it with the previous ones
        let elementToCompare = arr[i];
        for (let j = i - 1; j >= 0 && arr[j] > elementToCompare; j--) {
            arr[j + 1] = arr[j];
        }
        arr[j + 1] = elementToCompare;
    }
};

insertionSort([5, 3, 2, 1, 4]);
insertionSort([2, 5, 1, 9, 4, 11, 1, 7, 2, 16, 4, 1]);
insertionSort([2, 5, 8, 9, 4, 11, 12, 7, 2, 16, 4, 1]);
