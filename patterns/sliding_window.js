//Write a function that, given an unsorted array of integers and a number N, returns the biggest sum of a subset of N numbers.
//For example, ([1,5,2,6,3],3) should return [5,2,6]
const maxSubarraySum = (arr, n) => {
    if (arr.length < n) return null;
    if (n===1) return Math.max(...arr);

    let maxSum = 0;
    let currentSum;

    //We create the initial window
    for (let i=0; i<n; i++){
        maxSum+=arr[i];
    }

    currentSum=maxSum;

    //Then, we compare the other subsets
    for (let i=n; i<arr.length; i++){
        currentSum=currentSum + arr[i] - arr[i-n]; //We move the windows, but we keep its size
        maxSum = Math.max(currentSum, maxSum);
    }

    return maxSum;
}

console.log(maxSubarraySum([1,5,2,6,3],3));
console.log(maxSubarraySum([1,5,2,6,3],1));
console.log(maxSubarraySum([1,5,2,6,3],10));