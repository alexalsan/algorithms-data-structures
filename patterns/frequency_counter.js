//Write a function to check if, given 2 strings, the first one is an anagram of the second one
"use strict";

const validAnagram = (str1, str2) => {
  const fc1 = {};
  const fc2 = {};

  for (let character of str1) {
    fc1[character] = (fc1[character] || 0) + 1;
  }

  for (let character of str2) {
    fc2[character] = (fc2[character] || 0) + 1;
  }

  for (let character in fc1) {
      if (!fc2.hasOwnProperty(character) || fc1[character] !== fc2[character])
        return false;
  }

  return true;
};

console.log(validAnagram("cinema", "iceman")); //should be true
console.log(validAnagram("cat", "cat")); //should be true
console.log(validAnagram("azz", "zaa")); //should be false
console.log(validAnagram("awesome", "emwaso")); //should be false

//Write a function that, given a sorted array of integers, returns the number of values it contains.
//The repeated numeric values will only be counted once, so [-1,-1,-1, 2] will return 2.
const countUniqueValues = (arr) => {
  let fc1={};
  let uniqueValues = 0;

  for (int of arr) {
      fc1[int] = 1;
  }

  for (prop in fc1) {
      uniqueValues+=1;
  }

  return uniqueValues;
};

console.log(countUniqueValues([-1, -1, -1, -1, 2]));
console.log(countUniqueValues([-2, -1, -1, 0, 1, 1]));
console.log(countUniqueValues([]));
console.log(countUniqueValues([1, 1, 2, 3, 5, 5, 7, 8, 9, 9, 9, 11]));