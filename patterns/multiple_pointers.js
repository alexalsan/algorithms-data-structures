//Write a function that, given a sorted array of integers, returns the number of values it contains.
//The repeated numeric values will only be counted once, so [-1,-1,-1, 2] will return 2.
const countUniqueValues = (arr) => {
    if (arr.length < 2) return arr.length; //In case the array has 0 or 1 elements

    let i=0;

    for(let j=1; j<arr.length; j++) {
        if (arr[i] !== arr[j]){
            i++;
            arr[i] = arr[j];
        }
    }

    return i+1;
};

console.log(countUniqueValues([-1, -1, -1, -1, 2]));
console.log(countUniqueValues([-2, -1, -1, 0, 1, 1]));
console.log(countUniqueValues([]));
console.log(countUniqueValues([1, 1, 2, 3, 5, 5, 7, 8, 9, 9, 9, 11]));