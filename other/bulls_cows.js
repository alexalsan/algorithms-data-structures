"use strict";

/*
You are playing the following Bulls and Cows game with your friend: You write down a number and ask your friend to guess what the number is. Each time your friend makes a guess, you provide a hint that indicates how many digits in said guess match your secret number exactly in both digit and position (called "bulls") and how many digits match the secret number but locate in the wrong position (called "cows"). Your friend will use successive guesses and hints to eventually derive the secret number.

For example:
Secret number: "1807"
Friend's guess: "7810"

Hint: 1 bull and 3 cows. (The bull is 8, the cows are 0, 1 and 7.)
Write a function to return a hint according to the secret number and friend's guess, use A to indicate the bulls and B to indicate the cows. In the above example, your function should return "1A3B".
*/

const tryCode = (code, guess) => {
    let bulls = 0;
    let cows = 0;
    let codeCharCounter = {};
    let guessCharCounter = {};

    for (let i = 0; i < code.length; i++) {
        if (guess[i] === code[i]) bulls++;

        //We count how many times each character appears in the code and in the guess
        codeCharCounter[code[i]] = (codeCharCounter[code[i]] || 0) + 1;
        guessCharCounter[guess[i]] = (guessCharCounter[guess[i]] || 0) + 1;
    }

    for (let char in codeCharCounter)
        if (guessCharCounter[char])
            cows += Math.min(codeCharCounter[char], guessCharCounter[char]);

    cows -= bulls;
    return bulls + "A" + cows + "B";
};

console.log(tryCode("58255", "18521"));
