//Implement "map" and "filter" by using "reduce"
"use strict";

const array = [-4, -3, -1, 0, 3, 6, 7];

//Implementing filter
const filter = (arr, condition) =>{
    const filtered = arr.reduce((acc, element) => {
        if (condition(element)) {
            acc.push(element);
        }
        return acc;
    }, []);

    return filtered;
}

const isPositiveNumber = (x) => x>0;

const reduced = filter(array, isPositiveNumber);
console.log(reduced);

//Implementing map
const map = (arr, mapper) => {
    const mapped = arr.reduce( (acc, element) =>{
        acc.push(mapper(element));
        return acc;
    },[]);

    return mapped;
}

const multiplyNumber = (x) => x*x;

const reduced2 = map(array, multiplyNumber);
console.log(reduced2);