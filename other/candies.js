"use strict";
/*
There are N children standing in a line. Each child is assigned a rating value. You are giving candies to these children subjected to the following requirements:

1. Each child must have at least one candy.
2. Children with a higher rating get more candies than their neighbors.
*/

const howManyCandies = (ratings) => {
    //We see how many times we get each rating (there may be kids with the same rating)
    let frecuencyCounter = {};
    let nCandies = 0; //number of candies
    let lastAmountOfCandies = 0;

    for (let rating of ratings)
        frecuencyCounter[rating] = (frecuencyCounter[rating] || 0) + 1;

    //"frequencies" will be already sorted from the smaller number to the bigger
    //We give 1 candy to the kid/s with the smallest rating, and then 1 candy more to the next one
    for (let frec in frecuencyCounter){
        nCandies += frecuencyCounter[frec] * (lastAmountOfCandies+1);
        lastAmountOfCandies++;
    }

    return nCandies;
}

// const ratings = Array.from({length: 20}, () => Math.floor(Math.random() * 50));
const ratings = [3, 7, 1, 3, 9, 3, 1, 5, 6, 7];
console.log(ratings);
console.log(howManyCandies(ratings));