"use strict";
/*
There are N gas stations along a circular route, where the amount of gas at station i is gas[i].
You have a car with an unlimited gas tank and it costs cost[i] of gas to travel from station i to its next station (i+1).
You begin the journey with an empty tank at one of the gas stations.
Return the starting gas station's index if you can travel around the circuit once, otherwise return -1.
*/

const canITravel = (gas, cost) => {
    let startingStation = 0; //The station index
    let i = 0;
    let totalGasCost = 0;
    let solutionFound;

    while (startingStation < gas.length) {
        totalGasCost = totalGasCost + gas[i] - cost[i];
        
        if (totalGasCost < 0) {
            startingStation++;
            i = startingStation;
            totalGasCost = 0;
            solutionFound = false;
        } else {
            i = (i + 1) % gas.length;
            solutionFound = true;
            //We check if we have already completed a round
            if (i===startingStation) break;
        }
    }
    return solutionFound ? startingStation : -1;
};

const gas = [3, 5, 8, 9];
const cost = [4, 6, 4, 11];
console.log(canITravel(gas, cost));

// console.log(3%4);
