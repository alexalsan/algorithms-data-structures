"use strict";

class Graph {
    constructor() {
        this.adjacencyList = {};
    }

    addVertex = (vertex) => {
        if (!this.adjacencyList.hasOwnProperty(vertex))
            this.adjacencyList[vertex] = [];
    };

    removeVertex = (vertex) => {
        for (let v in this.adjacencyList) {
            if (v !== vertex) this.removeEdge(vertex, v);
        }
        delete this.adjacencyList[vertex];
    };

    addEdge = (nodeA, nodeB) => {
        if (
            !this.adjacencyList.hasOwnProperty(nodeA) ||
            !this.adjacencyList.hasOwnProperty(nodeB)
        ) {
            console.error("Both nodes must already exist");
            return;
        }
        this.adjacencyList[nodeA].push(nodeB);
        this.adjacencyList[nodeB].push(nodeA);
    };

    removeEdge = (nodeA, nodeB) => {
        this.adjacencyList[nodeA] = this.adjacencyList[nodeA].filter(
            (node) => node !== nodeB
        );
    };

    print = () => {
        console.log("-------------------------");
        for (let vertex in this.adjacencyList) {
            let allConnections = this.adjacencyList[vertex].reduce(
                (acc, currentValue) => acc + currentValue + " ",
                ""
            );
            console.log(vertex + " --> " + allConnections);
        }
        console.log("-------------------------");
    };

    // Traversal
    depthFirstRecursive = (node) => {
        const visited = {};
        const result = [];
        const adjacencyList = this.adjacencyList;

        (function dfr(n) {
            if (visited[n]) return;
            visited[n] = true;

            result.push(n);
            for (let neighbour of adjacencyList[n]) {
                dfr(neighbour);
            }
        })(node);
        return result;
    };

    depthFirstIterative = (node) => {
        const visited = {};
        const result = [];
        const toVisit = [node]; //nodes not visited yet

        while (toVisit.length) {
            const currentNode = toVisit.pop();
            if (visited[currentNode]) continue;
            visited[currentNode] = true;

            result.push(currentNode);
            for (let neighbour of this.adjacencyList[currentNode]) {
                toVisit.push(neighbour);
            }
        }
        return result;
    };

    breadthFirstIterative = (node) => {
        const visited = {};
        const result = [];
        const toVisit = [node]; //nodes not visited yet

        while (toVisit.length) {
            const currentNode = toVisit.shift();
            if (visited[currentNode]) continue;
            visited[currentNode] = true;

            result.push(currentNode);
            for (let neighbour of this.adjacencyList[currentNode]) {
                toVisit.push(neighbour);
            }
        }
        return result;
    };
}

// const g = new Graph();
// g.print();
// g.addVertex('Tokio');
// g.addVertex('Paris');
// g.addEdge('Tokio', 'Paris');
// g.print();
// g.removeEdge('Tokio', 'Paris');
// g.print();
// g.addVertex('Madrid');
// g.addVertex('Berlin');
// g.addVertex('Brasilia');
// g.addVertex('Rome');
// g.addVertex('London');
// g.addVertex('Lisbon');
// g.addEdge('Madrid', 'Paris');
// g.addEdge('Brasilia', 'Berlin');
// g.addEdge('Tokio', 'Rome');
// g.addEdge('Rome', 'Paris');
// g.addEdge('London', 'Lisbon');
// g.print();
// g.removeVertex('Tokio');
// g.print();

const g = new Graph();
g.addVertex("A");
g.addVertex("B");
g.addVertex("C");
g.addVertex("D");
g.addVertex("E");
g.addVertex("F");
g.addEdge("A", "B");
g.addEdge("A", "C");
g.addEdge("B", "D");
g.addEdge("C", "E");
g.addEdge("D", "E");
g.addEdge("D", "F");
g.addEdge("E", "F");
g.print();
console.log(g.depthFirstRecursive("A"));
console.log(g.depthFirstIterative("A"));
console.log(g.breadthFirstIterative("A"));