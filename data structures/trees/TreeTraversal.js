"use strict";

class BinarySearchTree {
    constructor() {
        this.root = null;
    }

    insert = (val) => {
        const newNode = new Node(val);

        if (this.root === null) {
            this.root = newNode;
            return this;
        }

        let currentNode = this.root;
        while (true) {
            if (val < currentNode.value) {
                if (currentNode.left === null) {
                    currentNode.left = newNode;
                    return this;
                }
                currentNode = currentNode.left;
            } else if (val > currentNode.value) {
                if (currentNode.right === null) {
                    currentNode.right = newNode;
                    return this;
                }
                currentNode = currentNode.right;
            } else return undefined;
        }
    };

    insertArray = (arr) => {
        arr.forEach((element) => this.insert(element));
    };

    print = () => this.printTree(this.root);

    printTree = (currentNode, nTabs = 0) => {
        if (currentNode === null) return;

        const tabsStr = "\t".repeat(nTabs);
        console.log(tabsStr + currentNode.value);

        this.printTree(currentNode.left, nTabs + 1);
        this.printTree(currentNode.right, nTabs + 1);
    };

    breadthFirstSearch = () => {
        const queue = [this.root];
        const visited = [];

        while (queue.length && queue[0] !== null) {
            visited.push(queue[0].value);

            if (queue[0].left !== null) queue.push(queue[0].left);

            if (queue[0].right !== null) queue.push(queue[0].right);

            queue.shift();
        }

        return visited;
    };

    depthFirstPreOrder = () => {
        const visited = [];

        const preOrderSearcher = (currentNode, visited) => {
            visited.push(currentNode.value);
            if(currentNode.left) preOrderSearcher(currentNode.left, visited);
            if(currentNode.right) preOrderSearcher(currentNode.right, visited);
        };

        preOrderSearcher(this.root, visited);
        return visited;
    };    

    depthFirstPostOrder = () => {
        const visited = [];

        const postOrderSearcher = (currentNode, visited) => {
            if(currentNode.left) postOrderSearcher(currentNode.left, visited);
            if(currentNode.right) postOrderSearcher(currentNode.right, visited);
            visited.push(currentNode.value);
        };

        postOrderSearcher(this.root, visited);
        return visited;
    };

    depthFirstInOrder = () => {
        const visited = [];

        const inOrderSearcher = (currentNode, visited) => {
            if(currentNode.left) inOrderSearcher(currentNode.left, visited);
            visited.push(currentNode.value);
            if(currentNode.right) inOrderSearcher(currentNode.right, visited);
        };

        inOrderSearcher(this.root, visited);
        return visited;
    };
}

class Node {
    constructor(val) {
        this.value = val;
        this.left = null;
        this.right = null;
    }
}

const bst = new BinarySearchTree();
bst.insert(10);
// bst.insert(3);
bst.insertArray([3, 6, 21, 44, 5, 12, 7, 88]);
// console.log(bst.root);
bst.print();
// console.log(bst.contains(4));

//SEARCH
// const values = bst.breadthFirstSearch();
// console.log(values);
const values = bst.depthFirstPreOrder();
console.log(values);

const values2 = bst.depthFirstInOrder();
console.log(values2);
