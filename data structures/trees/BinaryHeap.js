"use strict";

/*
    Given an element in the array with index N,
    the left child and right child are (2N+1) and (2N+2)
    and the parent element is (Math.floor( (N-1)/2) )
*/

class MaxBinaryHeap {
    constructor() {
        this.values = [];
    }

    swap = (a, b) => {
        const aux = this.values[a];
        this.values[a] = this.values[b];
        this.values[b] = aux;
    };

    insert = (val) => {
        this.values.push(val);

        let newElementIndex = this.values.length - 1;
        let parentIndex = Math.floor((newElementIndex - 1) / 2);

        //We check if the element is smaller than his parent. If not, we swap
        while (parentIndex >= 0) {
            if (this.values[newElementIndex] > this.values[parentIndex]) {
                this.swap(newElementIndex, parentIndex);
                newElementIndex = parentIndex;
                parentIndex = Math.floor((newElementIndex - 1) / 2);
            } else break;
        }
    };

    insertArray = (arr) => {
        arr.forEach((element) => this.insert(element));
    };

    remove = () => {
        if (!this.values.length) return null;

        this.swap(0, this.values.length - 1);
        const removedElement = this.values.pop();
        let currentIndex = 0;
        let leftChild, rightChild;

        while (true) {
            //We compare the current element with its left and right children
            leftChild = 2 * currentIndex + 1;
            rightChild = leftChild + 1;

            const isLeftChildGreater = (this.values[leftChild] && this.values[currentIndex] < this.values[leftChild]);
            const isRightChildGreater = (this.values[rightChild] && this.values[currentIndex] < this.values[rightChild]);

            //If the parent element is smaller than one or both of its children, we swap it with the biggest child
            if ( isLeftChildGreater || isRightChildGreater ) {
                if (this.values[rightChild] < this.values[leftChild]) {
                    this.swap(currentIndex, leftChild);
                    currentIndex = leftChild;
                } else {
                    this.swap(currentIndex, rightChild);
                    currentIndex = rightChild;
                }
            } else break;
        }

        return removedElement;
    };

    print = () => console.log(this.values);
}

const heap = new MaxBinaryHeap();
heap.insertArray([100, 19, 36, 25]);
heap.print();
heap.remove();
heap.print();
