"use strict";

class BinarySearchTree {
    constructor() {
        this.root = null;
    }

    insert = (val) => {
        const newNode = new Node (val);

        if (this.root === null){
            this.root = newNode;
            return this;
        }

        let currentNode = this.root;
        while (true) {
            if (val < currentNode.value){
                if (currentNode.left === null){
                    currentNode.left = newNode;
                    return this;
                }
                currentNode = currentNode.left;
            }
                
            else if (val > currentNode.value){
                if (currentNode.right === null){
                    currentNode.right = newNode;
                    return this;
                }
                currentNode = currentNode.right;
            }

            else
                return undefined;
        }
    };

    insertArray = (arr) => {
        arr.forEach(element => this.insert(element));
    }

    contains = (val) => {
        let currentNode = this.root;

        while (currentNode !== null){
            if (val < currentNode.value) {
                currentNode = currentNode.left;
            }
            else if (val > currentNode.value) {
                currentNode = currentNode.right;
            }
            else {
                return true;
            }
        }
        return false;
    }

    print = () => this.printTree(this.root);

    printTree = (currentNode, nTabs=0) => {
        if (currentNode === null) return;

        const tabsStr = "\t".repeat(nTabs); 
        console.log(tabsStr + currentNode.value);
        
        this.printTree(currentNode.left, nTabs+1);
        this.printTree(currentNode.right, nTabs+1);
    }
}

class Node {
    constructor(val) {
        this.value = val;
        this.left = null;
        this.right = null;
    }
}

const bst = new BinarySearchTree();
bst.insert(10);
// bst.insert(3);
bst.insertArray([3,6,21,44,5,12,7,88]);
// console.log(bst.root);
// bst.print();
console.log(bst.contains(4));
