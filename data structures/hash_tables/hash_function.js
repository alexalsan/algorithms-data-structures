"use strict";

//First version, it only works with strings & it's not of constant time
const hash = (key, arrLength) => {
    let total = 0;
    for (let char of key) {
        let value = char.charCodeAt(0);
        total = (total + value) % arrLength;
    }
    return total;
}

//Second version, using prime numbers and limiting the times we loop
const hash2 = (key, arrLength) => {
    let total = 0;
    const PRIME_NUMBER = 31;
    for (let i=0; i<Math.min(arrLength, 100); i++) {
        let value = char.charCodeAt(0);
        total = (total * PRIME_NUMBER + value) % arrLength;
    }
    return total;
}

console.log(hash("hi world!", 9), hash("pink", 9));