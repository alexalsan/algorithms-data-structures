class Node {
    constructor(val) {
        this.value = val;
        this.next = null;
    }
}

class Queue {
    constructor() {
        this.first = null;
        this.last = null;
        this.length = 0;
    }

    add = (val) => {
        const node = new Node(val);
        switch (this.length) {
            case 0:
                this.first = node;
                this.last = node;
                break;
            case 1:
                this.last = node;
                this.first.next = node;
                break;
            default:
                this.last.next = node;
                this.last = node;
                break;
        }
        this.length++;
        return this;
    };

    addArray = (arr) => {
        for (let i = 0; i < arr.length; i++) {
            this.add(arr[i]);
        }
    };

    remove = () => {
        switch (this.length) {
            case 0:
                return null;
            case 1:
                const oldfirst = this.first;
                this.first = null;
                this.last = null;
                this.length = 0;
                return oldfirst;
            default:
                const removedNode = this.first;
                const secondNode = this.first.next;
                this.first = secondNode;
                this.length--;
                return removedNode;
        }
    };

    print = () => {
        let currentNode = this.first;
        let printString = "" + currentNode.value;
        for (let i = 1; i < this.length; i++) {
            currentNode = currentNode.next;
            printString += " <--" + currentNode.value;
        }
        console.log(printString);
    };
}

const st = new Queue();
st.addArray([5,8,9,1,2,4]);
st.print();
st.remove();
st.add(7);
st.remove();
st.print();
