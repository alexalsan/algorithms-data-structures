"use strict";

class Node {
    constructor(val) {
        this.value = val;
        this.next = null;
    }
}

class SinglyLinkedList {
    constructor() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }

    push = (val) => {
        const node = new Node(val);
        switch (this.length) {
            case 0:
                this.head = node;
                this.tail = node;
                break;
            case 1:
                this.tail = node;
                this.head.next = node;
                break;
            default:
                this.tail.next = node;
                this.tail = node;
                break;
        }
        this.length++;
        return this;
    };

    pushArray = (arr) => {
        for (let i = 0; i < arr.length; i++) {
            this.push(arr[i]);
        }
    };

    pop = () => {
        switch (this.length) {
            case 0:
                return null;
            case 1:
                const oldHead = this.head;
                this.head = null;
                this.tail = null;
                this.length = 0;
                return oldHead;
            default:
                let poppedElement = this.head;
                let newTail;
                while (poppedElement.next) {
                    newTail = poppedElement;
                    poppedElement = poppedElement.next;
                }
                this.tail = newTail;
                this.tail.next = null;
                this.length--;
                return poppedElement;
        }
    };

    shift = () => {
        switch (this.length) {
            case 0:
                return null;
            case 1:
                const oldHead = this.head;
                this.head = null;
                this.tail = null;
                this.length = 0;
                return oldHead;
            default:
                const shiftedElement = this.head;
                const secondNode = this.head.next;
                this.head = secondNode;
                this.length--;
                return shiftedElement;
        }
    };

    unshift = (val) => {
        const node = new Node(val);
        switch (this.length) {
            case 0:
                this.head = node;
                this.tail = node;
                break;
            default:
                const oldHead = this.head;
                this.head = node;
                this.head.next = oldHead;
                break;
        }
        this.length++;
        return this;
    };
    
    reverse = () => {
        if (this.length < 2) return;
        let previousNode = this.head;
        let currentNode = this.head.next;
        let nextNode;

        this.tail = previousNode;
        while(currentNode) {
            nextNode = currentNode.next;
            currentNode.next = previousNode;
            previousNode = currentNode;
            currentNode = nextNode;
        }
        this.head = previousNode;
    }

    get = (index) => {
        if (index > this.length - 1 || this.length === 0) return null;
        else if (index === 0) return this.head;
        else if (index === this.length - 1) return this.tail;

        let currentNode = this.head;
        for (let i = 0; i < index; i++) {
            currentNode = currentNode.next;
        }
        return currentNode;
    };

    set = (index, val) => {
        if (index > this.length - 1 || this.length === 0) return null;

        let currentNode = this.head;
        for (let i = 0; i < index; i++) {
            currentNode = currentNode.next;
        }
        currentNode.value = val;
    };

    insert = (index, val) => {
        if (index > this.length || index < 0) return false;
        else if (index === 0) return !!this.unshift(val);
        else if (this.length === 0 || index === this.length) return !!this.push(val);

        const node = new Node(val);
        let previousNode = this.head;
        for (let i = 1; i < index; i++) {
            previousNode = previousNode.next;
        }
        const nodeToMove = previousNode.next;
        previousNode.next = node;
        node.next = nodeToMove;
        this.length++;
        return true;
    };

    remove = (index) => {
        if (index > this.length - 1 || this.length === 0) return null;
        else if (index === 0) return this.shift();
        else if (index === this.length - 1) return this.pop();

        let previousNode = this.head;
        for (let i = 1; i < index; i++) {
            previousNode = previousNode.next;
        }
        const removedNode = previousNode.next;
        previousNode.next = removedNode.next;
        this.length--;
        return removedNode;
    };

    print = () => {
        let currentNode = this.head;
        let printString = "" + currentNode.value;
        for (let i = 1; i < this.length; i++) {
            currentNode = currentNode.next;
            printString += "--> " + currentNode.value;
        }
        console.log(printString);
    };
}

const sll = new SinglyLinkedList();
// sll.push(4);
// sll.push(6);
// sll.push(5);
// sll.push(7);
sll.pushArray([4, 2, 5, 1, 5, 6, 7, 8]);
sll.print();
sll.insert(4, 18);
sll.print();
sll.reverse();
sll.print();
