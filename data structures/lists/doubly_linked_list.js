"use strict";

class Node {
    constructor(val) {
        this.value = val;
        this.next = null;
        this.prev = null;
    }
}

class DoublyLinkedList {
    constructor() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }

    push = (val) => {
        const node = new Node(val);
        switch (this.length) {
            case 0:
                this.head = node;
                this.tail = node;
                break;
            case 1:
                this.tail = node;
                this.head.next = node;
                this.tail.prev = this.head;
                break;
            default:
                const oldTail = this.tail;
                oldTail.next = node;
                this.tail = node;
                this.tail.prev = oldTail;
                break;
        }
        this.length++;
        return this;
    };

    pushArray = (arr) => {
        for (let i = 0; i < arr.length; i++) {
            this.push(arr[i]);
        }
    };

    pop = () => {
        //TODO
    };

    shift = () => {
        //TODO
    };

    unshift = (val) => {
        //TODO
    };

    reverse = () => {
        if (this.length < 2) return;

        let currentNode = this.head;
        this.tail = this.head;

        while (currentNode) {
            //We swap "next" and "prev" pointers
            let aux = currentNode.next;
            currentNode.next = currentNode.prev;
            currentNode.prev = aux;

            //If we reach the last element
            if (!currentNode.prev) this.head = currentNode;

            currentNode = currentNode.prev;
        }
    };

    get = (index) => {
        if (index > this.length - 1 || this.length === 0) return null;
        else if (index === 0) return this.head;
        else if (index === this.length - 1) return this.tail;

        let currentNode;

        //If the index is closer to the head, we start looking from the beginning
        if (index < Math.floor(this.length / 2)) {
            currentNode = this.head;
            for (let i = 0; i < index; i++) {
                currentNode = currentNode.next;
            }
        }
        //If the index is closed to the tail, we search backwards
        else {
            currentNode = this.tail;
            for (let i = 0; i < this.length-1-index; i++) {
                currentNode = currentNode.prev;
            }
        }
        return currentNode;
    };

    set = (index, val) => {
        //TODO
    };

    insert = (index, val) => {
        //TODO
    };

    remove = (index) => {
        //TODO
    };

    print = () => {
        let currentNode = this.head;
        let printString = "" + currentNode.value;
        for (let i = 1; i < this.length; i++) {
            currentNode = currentNode.next;
            printString += " <--> " + currentNode.value;
        }
        console.log(printString);
    };
}

const dll = new DoublyLinkedList();
// dll.push(4);
// dll.push(6);
// dll.push(5);
// dll.push(7);
// dll.print();
dll.pushArray([4, 2, 5, 1, 5, 6, 7, 8]);
dll.print();
console.log(dll.get(3).value);
console.log(dll.get(6).value);

// dll.print();
// dll.insert(4, 18);
// dll.print();
dll.reverse();
dll.print();
