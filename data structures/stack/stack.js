class Node {
    constructor(val) {
        this.value = val;
        this.prev = null;
    }
}

class Stack {
    constructor() {
        this.first = null;
        this.last = null;
        this.length = 0;
    }

    add = (val) => {
        const node = new Node(val);
        if (this.length === 0) {
            this.first = node;
            this.last = node;
        } else {
            const oldLast = this.last;
            this.last = node;
            this.last.prev = oldLast;
        }
        this.length++;
        return this;
    };

    addArray = (arr) => {
        for (let i = 0; i < arr.length; i++) {
            this.add(arr[i]);
        }
    };

    remove = () => {
        if (this.length < 1) return null;
        const removedNode = this.last;
        this.last = removedNode.prev;
        this.length--;
        return removedNode;
    };

    print = () => {
        let currentNode = this.last;
        let printString = "" + currentNode.value;
        for (let i = 1; i < this.length; i++) {
            currentNode = currentNode.prev;
            printString += "--> " + currentNode.value;
        }
        console.log(printString);
    };
}

const st = new Stack();
st.addArray([5,8,9,1,2,4]);
st.print();
st.remove();
st.remove();
st.print();
