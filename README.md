# Algorithms & Data structures #

This is a repository where I will implement in JavaScript some of the most common algorithms and data structures.

## What can be found in the repository?
* Searching algorithms
* Sorting algorithms
* Some common patterns to solve programming problems

## Incoming
* More sorting algorithms (merge, quick,...)
* Data structures (trees, lists, hash tables,...)