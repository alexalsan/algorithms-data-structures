function binarySearch(arr, value){
    let leftIndex = 0;
    let rightIndex = arr.length-1;

    while(leftIndex+1 < rightIndex) {
        if (arr[leftIndex] === value) return leftIndex;
        if (arr[rightIndex] === value) return rightIndex;

        const halfpoint = leftIndex + Math.floor((rightIndex-leftIndex)/2);

        if (arr[halfpoint] === value)
            return halfpoint;
        else if (value < arr[halfpoint]) {
            rightIndex = halfpoint;
        }
        else if (value > arr[halfpoint]) {
            leftIndex = halfpoint;
        }
    }
    
    return -1;
}

console.log( binarySearch([1,2,3,4,5],2) ) //should be 1
console.log( binarySearch([1,2,3,4,5],3) ) //should be 2
console.log( binarySearch([1,2,3,4,5],5) ) //should be 4
console.log( binarySearch([5, 6, 10, 13, 14, 18, 30, 34, 35, 37, 40, 44, 64, 79, 84, 86, 95, 96, 98, 99], 10) ) //should be 2
console.log( binarySearch([5, 6, 10, 13, 14, 18, 30, 34, 35, 37, 40, 44, 64, 79, 84, 86, 95, 96, 98, 99], 95) ) //should be 16